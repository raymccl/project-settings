// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	const workspaceFolders = vscode.workspace.workspaceFolders || [];
	const config = vscode.workspace.getConfiguration('project');
	const themes : Theme[] = config.get("settings") || [];

	if (workspaceFolders.length === 1) {
		checkThemes(workspaceFolders[0].uri.path, themes);
	} else {
		console.log('Unfortunately this extension doesn\'t support multiple workspaces at the moment');
	}
}

// this method is called when your extension is deactivated
export function deactivate() {}

function checkThemes (path: string, themes : Theme[]) {
	for (let i = 0; i < themes.length; i++) {
		const pathRegex : RegExp = new RegExp(themes[i].path);

		if (pathRegex.test(path)) {
			applyRules(themes[i].rules);
			break;
		}
	}
}

function applyRules (rules : Theme["rules"]) {
	for (let k in rules) {
		setConfig(k, rules[k], false);
	}
}

function setConfig (section: string, value: any, global: boolean = false) {
	return vscode.workspace.getConfiguration().update(section, value, global);
}