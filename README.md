# project-settings

Enables folder specific project settings. As workspace files can be associated with a repository, this extension enables workspace-level user settings.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

Initial release

**Enjoy!**
